<div id="top"></div>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
<!-- [![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url] -->
<!-- [![LinkedIn][linkedin-shield]][linkedin-url] -->



<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/dpiofog/kdrt-frontend">
    <img src="flutter_frontend/assets/logo/logo_512.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Angelica</h3>

  <p align="center">
    A companion app to help victims of domestic violence
    <!-- <br />
    <a href="https://github.com/othneildrew/Best-README-Template"><strong>Explore the docs »</strong></a> -->
    <!-- <br />
    <br />
    <a href="https://github.com/othneildrew/Best-README-Template">View Demo</a>
    ·
    <a href="https://github.com/othneildrew/Best-README-Template/issues">Report Bug</a>
    ·
    <a href="https://github.com/othneildrew/Best-README-Template/issues">Request Feature</a> -->
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <!-- <li><a href="#roadmap">Roadmap</a></li> -->
    <!-- <li><a href="#contributing">Contributing</a></li> -->
    <!-- <li><a href="#license">License</a></li> -->
    <li><a href="#members">Members</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

Domestic violence or KDRT in Indonesian is defined as an act of violence physically or mentally to the victim by aggression among people with kinship. The victim in the family could be anyone such as children, women, men, or the elderly. Domestic violence issues can range from minor impact like humiliation to concerning impact such as sexual abusement (Holt, 2008). In reality, it doesn't matter if it's only small things like humiliation, it still gives a bad impact to the victim and it won’t guarantee it will not escalate to the next level. In the end the victim will be left with posttraumatic stress and insecurity that could be carried for the rest of their life. 

Due to the COVID-19 pandemic resulting in some countries conducting lockdown for their citizens, there has been an increase of reports on domestic violence. For example, there has been an increase of 40% in reported domestic violence in Brazil (Bradbury-Jones, 2020). This incident happens because citizens need to battle the pandemic, which in turn isolates them alongside their abusers, increasing the frequency of experiencing domestic violence  (Kofman, 2020). A big concern is when the victims become reluctant or unable to show evidence when reporting the acts of violence that have befallen them.

Our proposed solution, as to be our project, is a device that would be capable of discreetly collecting evidence of domestic violence by way of audio and video recording. In order to minimize misuse of the device and save storage space, it will only start recording and storing the evidence if it detects the signs of domestic violence. The microphone will act as a sensor for audio queues where an AI will be used to determine if the sounds that it picks up is associated with domestic violence. If it does, the camera will be turned on and record the whole situation. The camera will also be equipped with an AI that can detect objects that it sees as to provide additional evidence. All recordings will be stored in cloud storage and the device itself will only store the latest recordings if it does not have enough space.

<div align="right">(<a href="#top">back to top</a>)</div>



### Built With

This repository holds the frontend section of the whole project. It is a mobile app that was created using [Flutter](https://flutter.dev/).

<div align="right">(<a href="#top">back to top</a>)</div>



<!-- GETTING STARTED -->
## Getting Started

If you would like to try out the finished application, you can download the app's APK from [here](https://dpiofog-bucket.s3.ap-southeast-1.amazonaws.com/app.apk). _Note: only available for Android devices._

To get a local copy of the project up and running instead, follow these simple steps.

### Prerequisites

[Flutter](https://docs.flutter.dev/get-started/install/windows) needs to be installed, alongside a choice of your preferred IDE. [Android Studio](https://docs.flutter.dev/get-started/install/windows#android-setup) is recommended.

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/dpiofog/kdrt-frontend.git
   ```
2. Open `flutter_frontend` project using your IDE.
3. If there are errors due to missing imports, run the following command in the terminal from the project folder
   ```sh
   flutter pub get
   ```
4. Connect your IDE to a mobile device (via emulator or your plugged in device) and hit
   ```sh
   flutter run
   ```
   This will launch the mobile app on your device.

<div align="right">(<a href="#top">back to top</a>)</div>



<!-- USAGE EXAMPLES -->
## Usage

The purpose of this mobile app is to act as a companion app to view the videos recorded by your [Angelica's Raspberry Pi](https://gitlab.com/dpiofog/kdrt-raspberrypi). New videos are recorded whenever it detects signs of domestic violence or abuse, and you can view the footage using this app.

When you first open the app, you will be greated with the landing page. From here, you need to either register a new account or sign in.

<div align="center">
  <table class="tableImageCaption">
    <tr>
      <td> <img src="./screenshots/landing.png" width=180> </td>
      <td> <img src="./screenshots/register.png" width=180> </td>
      <td> <img src="./screenshots/login.png" width=180> </td>
    </tr>
    <tr>
      <td> <p align="center"><i>Landing page</i></p> </td>
      <td> <p align="center"><i>Register</i></p> </td>
      <td> <p align="center"><i>Login</i></p> </td>
    </tr>
  </table>
</div>

If this is your first time using the app, you will be greeted with an empty home screen. Tap on the big plus icon or the `Tap here to add` link to register a device. Otherwise, it will display your device and its recordings. You can also tap on the `View Activity` button to see a trend of your recordings on a daily basis.

<div align="center">
  <table class="tableImageCaption">
    <tr>
      <td> <img src="./screenshots/home-no-device.png"  width=180> </td>
      <td> <img src="./screenshots/register-device.png"  width=180> </td>
      <td> <img src="./screenshots/home.png"  width=180> </td>
      <td> <img src="./screenshots/activity_history.png"  width=180> </td>
    </tr>
    <tr>
      <td> <p align="center"><i>Home (no registered device)</i></p> </td>
      <td> <p align="center"><i>Register device</i></p> </td>
      <td> <p align="center"><i>Home (with registered device)</i></p> </td>
      <td> <p align="center"><i>User activity trend</i></p> </td>
    </tr>
  </table>
</div>

From here, you may tap on any of the list items and it will open a screen to display the video footage. Below the video lists the objects detected in the video and their count.

<div align="center">
  <table class="tableImageCaption">
    <tr>
      <td> <img src="./screenshots/video.png" width=180> </td>
    </tr>
    <tr>
      <td> <p align="center"><i>Recording and detected objects</i></p> </td>
    </tr>
  </table>
</div>

If the list is empty, it could mean that our [AI](https://gitlab.com/dpiofog/kdrt-backend-object-detection) was not able to detect any objects or is currently still processing the footage. We also provide the feature to share the video by tapping on the share icon on the top right corner of the screen.

We hope that this app can be used to aid victims of domestic violence and abuse.

<div align="right">(<a href="#top">back to top</a>)</div>



<!-- MEMBERS -->
## Members

- [Gardyan Akbar](https://gitlab.com/giantsweetroll)
- [Eric Edgari](https://gitlab.com/Trutina1220)
- [Vincentius Gabriel](https://gitlab.com/kronmess)
- [Jocelyn Thiojaya](https://gitlab.com/jocelynthiojaya)

Project Link: [https://gitlab.com/dpiofog/](https://gitlab.com/dpiofog/)

<div align="right">(<a href="#top">back to top</a>)</div>



<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

_Flutter packages used in the making of the project_
* [charts_flutter](https://pub.dev/packages/charts_flutter)
* [chewie](https://pub.dev/packages/chewie)
* [cloud_firestore](https://pub.dev/packages/cloud_firestore)
* [fake_cloud_firestore](https://pub.dev/packages/fake_cloud_firestore)
* [firebase_auth](https://pub.dev/packages/firebase_auth)
* [firebase_auth_mocks](https://pub.dev/packages/firebase_auth_mocks)
* [firebase_core](https://pub.dev/packages/firebase_core)
* [flutter_cache_manager](https://pub.dev/packages/flutter_cache_manager)
* [flutter_icons](https://pub.dev/packages/flutter_icons)
* [flutter_launcher_icons](https://pub.dev/packages/flutter_launcher_icons)
* [flutter_settings_ui](https://pub.dev/packages/flutter_settings_ui)
* [flutter_spinkit](https://pub.dev/packages/flutter_spinkit)
* [fluttertoast](https://pub.dev/packages/fluttertoast)
* [google_fonts](https://pub.dev/packages/google_fonts)
* [intl](https://pub.dev/packages/intl)
* [provider](https://pub.dev/packages/provider)
* [share_plus](https://pub.dev/packages/share_plus)
* [video_player](https://pub.dev/packages/video_player)

_Works cited_
* Bradbury‐Jones, C., & Isham, L. (2020). The pandemic paradox: The consequences of
	COVID‐19 on domestic violence. Journal of Clinical Nursing, 29(13–14), 2047–
	1.    [https://doi.org/10.1111/jocn.15296](https://doi.org/10.1111/jocn.15296)
	children and young people: A review of the literature. Child Abuse & Neglect, 32(8), 
	797–810. [https://doi.org/10.1016/j.chiabu.2008.02.004](https://doi.org/10.1016/j.chiabu.2008.02.004)
* Kofman, Y. B., & Garfin, D. R. (2020). Home is not always a haven: The domestic violence crisis amid the COVID-19 pandemic. Psychological Trauma: Theory, Research, Practice, and Policy, 12(S1), S199.

<div align="right">(<a href="#top">back to top</a>)</div>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
<!-- [contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: https://github.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues -->
<!-- [license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge -->
<!-- [license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew -->
<!-- [product-screenshot]: images/screenshot.png -->
