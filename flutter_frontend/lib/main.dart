import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_frontend/screens/wrapper.dart';
import 'package:flutter_frontend/services/auth.dart';
import 'package:flutter_frontend/shared/custom_theme.dart';
import 'package:flutter_frontend/shared/screen_state.dart';
import 'package:provider/provider.dart';

void main() async {

  LicenseRegistry.addLicense(() async* {
    final license = await rootBundle.loadString('assets/fonts/poppins/OFL.txt');
    yield LicenseEntryWithLineBreaks(['google_fonts'], license);
  });
  LicenseRegistry.addLicense(() async* {
    final license = await rootBundle.loadString('assets/fonts/dmsans/OFL.txt');
    yield LicenseEntryWithLineBreaks(['google_fonts'], license);
  });

  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp, DeviceOrientation.portraitDown
  ]);

  await Firebase.initializeApp();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  Widget buildMainWidget() => MaterialApp(
    debugShowCheckedModeBanner: false,
    theme: CustomTheme.lightTheme,
    home: const Wrapper(),
  );

  @override
  Widget build(BuildContext context) {
    return StreamProvider<User?>.value(
      value: AuthService().user,
      initialData: null,
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider<ScreenState>(
            create: (_) => ScreenState(),
          )
        ],
        child: buildMainWidget(),
      ),
    );
  }
}
