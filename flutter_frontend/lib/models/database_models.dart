class DeviceData {
  String userId, deviceName, deviceId;

  DeviceData({
    required this.deviceId,
    this.userId = '',
    this.deviceName = '',
  });
}

class UserSettingData {
  bool enableSystem;
  bool enableMobileData;

  UserSettingData({
    this.enableSystem = true,
    this.enableMobileData = false,
  });
}

class HistoryItem {
  String userId, videoLink, id, deviceId;
  DateTime date;
  List<String> detectedObjects;
  // Duration videoDuration;
  String videoDuration;
  bool isDeleted;

  HistoryItem({
    required this.id,
    required this.userId,
    required this.videoLink,
    required this.date,
    required this.deviceId,
    this.detectedObjects = const [],
    this.videoDuration = '0:00:00',
    this.isDeleted = false,
  });
}