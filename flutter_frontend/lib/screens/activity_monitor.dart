import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter_frontend/models/database_models.dart';
import 'package:flutter_frontend/shared/utility_functions.dart';

class ActivityMonitor extends StatelessWidget {

  final List<HistoryItem> histories;

  // Constructor
  const ActivityMonitor({
    required this.histories,
    Key? key,
  }) : super(key: key);

  // Private methods
  /// Get number of entries per datea
  List<DateCount> getEntriesCountPerDate(List<HistoryItem> histories) {

    // Reverse the date
    histories = histories.reversed.toList();

    List<DateCount> list = [];

    // Map date to count
    Map<String, int> map = {};
    for (HistoryItem history in histories) {
      String dateString = formatDateNoTime(history.date);

      if (map.containsKey(dateString)) {
        map[dateString] = map[dateString]! + 1;
      } else {
        map[dateString] = 1;
      }
    }

    // Convert map to class
    for (String key in map.keys) {
      list.add(DateCount(date: key, count: map[key]!));
    }

    return list;
  }

  // Overridden Methods
  @override
  Widget build(BuildContext context) {

    // Convert history list to series list
    List<charts.Series<dynamic, String>> data = [];

    charts.Series<dynamic, String> series = charts.Series<DateCount, String>(
      id: 'Recordings',
      data: getEntriesCountPerDate(histories),
      domainFn: (DateCount dc, _) => dc.date,
      measureFn: (DateCount dc, _) => dc.count,
      colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault
    );

    data.add(series);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Activity'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: SizedBox(
          width: 500,
          child: charts.BarChart(
            data,
            animate: false,
          ),
        ),
      ),
    );
  }
}

class DateCount {
  String date;
  int count;

  DateCount({
    required this.date,
    required this.count,
  });
}