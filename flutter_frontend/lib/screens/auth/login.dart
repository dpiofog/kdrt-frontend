import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_frontend/screens/auth/register.dart';
import 'package:flutter_frontend/services/auth.dart';
import 'package:flutter_frontend/services/input_validators.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/screen_state.dart';
import 'package:flutter_frontend/shared/utility_functions.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'dart:developer' as developer;

import 'package:google_fonts/google_fonts.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  final formKey = GlobalKey<FormState>();

  String email = '',
      password = '',
      error = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: ListView(
          children: [
            const SizedBox(height: 50,),
            Text(
              'Sign In',
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline1,
            ),
            const SizedBox(height: 10,),
            Wrap(
              alignment: WrapAlignment.center,
              children: [
                Text(
                  'Not Registered?',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                const SizedBox(width: 5, height: 10,),
                InkWell (
                  child: Text(
                    'Sign up here.',
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: hyperlinkColor,
                    ),
                  ),
                  onTap: () {
                    Navigator.pushReplacement(
                        context,
                        PageRouteBuilder(
                          pageBuilder: (pageBuilder, anim1, anim2) =>
                          const RegisterScreen(),
                          reverseTransitionDuration: const Duration(seconds: 0),
                        )
                    );
                  },
                ),
              ],
            ),
            const SizedBox(height: 20,),
            Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  createDefaultInputHint(
                      context: context,
                      text: 'EMAIL'
                  ),
                  const SizedBox(height: 5,),
                  TextFormField(
                    key: const Key('emailTF'),
                    initialValue: email,
                    onChanged: (val) => email = val,
                    validator: EmailFieldValidator.validate,
                    decoration: inputTextDecoration,
                  ),
                  const SizedBox(height: 5,),
                  createDefaultInputHint(
                      context: context,
                      text: 'PASSWORD'
                  ),
                  const SizedBox(height: 5,),
                  TextFormField(
                    key: const Key('passwordTF'),
                    initialValue: password,
                    onChanged: (val) => password = val,
                    validator: PasswordFieldValidator.validate,
                    obscureText: true,
                    decoration: inputTextDecoration,
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            if (error != '') Text(
              error,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                color: Colors.red,
              ),
            ),
            Row(
              children: [
                Expanded(
                  child: ElevatedButton(
                    child: Text(
                      'Sign In',
                      style: GoogleFonts.poppins(
                        textStyle: Theme.of(context).textTheme.bodyText1!.copyWith(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    onPressed: () async {
                      setState(() {
                        error = '';
                      });
                      showLoading(context);
                      if (formKey.currentState!.validate()) {
                        try {
                          AuthService auth = AuthService();
                          dynamic response = await auth.signIn(email, password);
                          Navigator.pop(context);   // Pop loading screen

                          if (response != null) {
                            Navigator.pop(context);   // Pop to landing screen
                            setScreen(context, Screen.home);
                          } else {
                            setState(() {
                              error = 'Cannot login with those credentials';
                            });
                          }
                        } catch(e) {
                          developer.log(e.toString(), name: 'login.dart');
                          Navigator.pop(context);   // Pop loading screen
                          setState(() {
                            if (e is FirebaseAuthException) {
                              switch (e.code) {
                                case "invalid-email":
                                  error = 'Invalid email or password';
                                  break;

                                case "wrong-password":
                                  error = 'Invalid email or password';
                                  break;

                                case "user-not-found":
                                  error = 'The following user cannot be found';
                                  break;

                                case "user-disabled":
                                  error = 'The following user cannot be found';
                                  break;

                                default:
                                  error = 'Something went wrong please try again';
                                  break;
                              }
                            } else {
                              error = 'Something went wrong please try again';
                            }
                          });
                        }
                      } else {
                        Navigator.pop(context);   // Pop loading
                      }
                    },
                  ),
                ),
              ],
            ),
            const SizedBox(height: 20,),
            InkWell (
              child: Text(
                'Forgot password',
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: hyperlinkColor,
                ),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  createFadingPageRouteBuilder(child: const ResetPassword())
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

class ResetPassword extends StatefulWidget {
  const ResetPassword({Key? key}) : super(key: key);

  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  final formKey = GlobalKey<FormState>();

  String email = '', error = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Reset Password'
        ),
      ),
      body: Form(
        key: formKey,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
          child: ListView(
            children: [
              createDefaultInputHint(
                context: context,
                text: 'Email',
              ),
              const SizedBox(height: 5,),
              TextFormField(
                key: const Key('emaiLTF'),
                initialValue: email,
                onChanged: (val) => email = val,
                validator: EmptyFieldValidator.validate,
                decoration: inputTextDecoration.copyWith(
                  hintText: 'Enter your email'
                ),
              ),
              const SizedBox(height: 10,),
              if (error != '') Text(
                error,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: Colors.red,
                ),
              ),
              Row(
                children: [
                  Expanded(
                    child: ElevatedButton(
                      key: const Key('resetEmailButton'),
                      onPressed: () async {
                        setState(() {
                          error = '';
                        });
                        showLoading(context);
                        if (formKey.currentState!.validate()) {
                          AuthService auth = AuthService();

                          await auth.resetPassword(email).then((value) {
                            Navigator.pop(context);
                            Navigator.pop(context);
                            Fluttertoast.showToast(msg: 'Password reset email sent');
                          }).catchError((e) {
                            Navigator.pop(context);
                            developer.log(
                              'Unable to send password reset email:',
                              name: 'login.dart',
                              error: e
                            );
                            setState(() {
                              error = e.message;
                            });
                          });
                        } else {
                          Navigator.pop(context);   // Pop Loading
                        }
                      },
                      child: Text(
                        'Send',
                        style: GoogleFonts.poppins(
                          textStyle: Theme.of(context).textTheme.bodyText1!.copyWith(
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
