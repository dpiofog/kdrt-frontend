import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_frontend/screens/auth/login.dart';
import 'package:flutter_frontend/services/auth.dart';
import 'package:flutter_frontend/services/input_validators.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';
import 'package:google_fonts/google_fonts.dart';

import 'dart:developer' as developer;

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {

  final formKey = GlobalKey<FormState>();
  String username = '';
  String email = '';
  String password = '';
  String error = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: ListView(
          key: const Key('listView'),
          children: [
            const SizedBox(height: 50,),
            Text(
              'Create New Account',
              key: const Key('createNewAccount'),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline1,
            ),
            const SizedBox(height: 10,),
            Wrap(
              key: const Key('wrap'),
              alignment: WrapAlignment.center,
              children: [
                Text(
                  'Already Registered?',
                  key: const Key('alreadyRegistered'),
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                const SizedBox(width: 5, height: 10,),
                InkWell(
                  key: const Key('inkWell'),
                  child: Text(
                    'Sign in here.',
                    key: const Key('signInHere'),
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: hyperlinkColor,
                    ),
                  ),
                  onTap: () {
                    Navigator.pushReplacement(
                      context,
                      PageRouteBuilder(
                        pageBuilder: (pageBuilder, anim1, anim2) =>
                        const LoginScreen(
                          key: Key('loginScreen'),
                        ),
                        reverseTransitionDuration: const Duration(seconds: 0),
                      ),
                    );
                  },
                ),
              ],
            ),
            const SizedBox(height: 20,),
            Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  createDefaultInputHint(
                    key: const Key('usernameHint'),
                    context: context,
                    text: 'USERNAME',
                  ),
                  const SizedBox(height: 5,),
                  TextFormField(
                    key: const Key('usernameTF'),
                    initialValue: username,
                    onChanged: (val) => username = val,
                    validator: EmptyFieldValidator.validate,
                    decoration: inputTextDecoration,
                  ),
                  const SizedBox(height: 5,),
                  createDefaultInputHint(
                    key: const Key('emailHint'),
                    context: context,
                    text: 'EMAIL'
                  ),
                  const SizedBox(height: 5,),
                  TextFormField(
                    key: const Key('emailTF'),
                    initialValue: email,
                    onChanged: (val) => email = val,
                    validator: EmailFieldValidator.validate,
                    decoration: inputTextDecoration,
                  ),
                  const SizedBox(height: 5,),
                  createDefaultInputHint(
                    key: const Key('passwordHint'),
                    context: context,
                    text: 'PASSWORD'
                  ),
                  const SizedBox(height: 5,),
                  TextFormField(
                    key: const Key('passwordTF'),
                    initialValue: password,
                    onChanged: (val) => password = val,
                    validator: PasswordFieldValidator.validate,
                    obscureText: true,
                    decoration: inputTextDecoration,
                  ),
                  const SizedBox(height: 5,),
                  createDefaultInputHint(
                    key: const Key('confirmPasswordHint'),
                    context: context,
                    text: 'CONFIRM PASSWORD'
                  ),
                  const SizedBox(height: 5,),
                  TextFormField(
                    key: const Key('confirmPasswordTF'),
                    initialValue: '',
                    validator: (val) => ConfirmPasswordFieldValidator.validate(val, password),
                    obscureText: true,
                    decoration: inputTextDecoration,
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            if (error != '') Text(
              error,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                color: Colors.red,
              ),
            ),
            Row(
              children: [
                Expanded(
                  child: ElevatedButton(
                    key: const Key('signUpButton'),
                    child: Text(
                      'Sign Up',
                      style: GoogleFonts.poppins(
                        textStyle: Theme.of(context).textTheme.bodyText1!.copyWith(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    onPressed: () async {
                      setState(() {
                        error = '';
                      });
                      showLoading(context);
                      if (formKey.currentState!.validate()) {
                        try {
                          AuthService auth = AuthService();
                          dynamic result = await auth.register(
                              email: email,
                              password: password,
                              username: username,
                          );
                          Navigator.pop(context);   // Pop loading screen
                          if (result != null) {
                            await showDialog
                              (context: context,
                              builder: (context) => AlertDialog(
                                title: Text(
                                  'Finish Registration',
                                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                    fontWeight: FontWeight.bold
                                  ),
                                ),
                                content: Text(
                                  'An email has been sent to your email address to verify your account',
                                  style: Theme.of(context).textTheme.bodyText1,
                                ),
                                actions: [
                                  TextButton(
                                    child: const Text('OK'),
                                    onPressed: () => Navigator.pop(context),
                                  )
                                ],
                              ),
                            );
                            Navigator.pushReplacement(
                                context,
                                PageRouteBuilder(
                                  pageBuilder: (pageBuilder, anim1, anim2) =>
                                  const LoginScreen(
                                    key: Key('loginScreen'),
                                  ),
                                  reverseTransitionDuration: const Duration(seconds: 0),
                                )
                            );
                          } else {
                            Navigator.pop(context); // Pop loading screen
                            setState(() {
                              error = 'Something went wrong, please try again';
                            });
                          }
                        } catch (e) {
                          developer.log(
                            e.toString(),
                            name: 'register.dart - registering new user'
                          );
                          setState(() {
                            if (e is FirebaseAuthException) {
                              switch (e.code) {
                                case 'email-already-in-use':
                                  error = 'Email is not available';
                                  break;

                                case 'invalid-email':
                                  error = 'Invalid email';
                                  break;

                                case 'weak-password':
                                  error = 'Password is too weak';
                                  break;

                                default:
                                  error = 'Something went wrong, please try again';
                                  break;
                              }
                            }
                          });
                        }
                      } else {
                        Navigator.pop(context);   // Pop Loading Screen
                      }
                    },
                  ),
                ),
              ],
            )
          ],
        ),
      )
    );
  }
}
