import 'package:flutter/material.dart';
import 'package:flutter_frontend/services/database.dart';
import 'package:flutter_frontend/services/input_validators.dart';

import 'dart:developer' as developer;

import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:fluttertoast/fluttertoast.dart';

class RegisterDeviceScreen extends StatefulWidget {

  final String userId;

  const RegisterDeviceScreen({
    Key? key,
    required this.userId,
  }) : super(key: key);

  @override
  _RegisterDeviceScreenState createState() => _RegisterDeviceScreenState();
}

class _RegisterDeviceScreenState extends State<RegisterDeviceScreen> {

  final formKey = GlobalKey<FormState>();

  String deviceID = '',
      deviceName = '',
      error = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(appName),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: ListView(
          children: [
            const SizedBox(height: 50,),
            Text(
              'Connect to Device',
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline1,
            ),
            const SizedBox(height: 10,),
            // Wrap(
            //   alignment: WrapAlignment.center,
            //   children: [
            //     Text(
            //       'Nevermind,',
            //       style: Theme.of(context).textTheme.bodyText1,
            //     ),
            //     const SizedBox(width: 5, height: 10,),
            //     InkWell(
            //       child: Text(
            //         'Sign out.',
            //         style: Theme.of(context).textTheme.bodyText1!.copyWith(
            //           color: hyperlinkColor,
            //         ),
            //       ),
            //       onTap: () async {
            //         showLoading(context);
            //         developer.log('Signing out user...', name: 'settings.dart');
            //         try {
            //           AuthService auth = AuthService();
            //           await auth.signOut().then((value) {
            //             Navigator.pop(context); // Pop loading
            //           });
            //         } catch(e) {
            //           developer.log('Unable to sign out user: ${e.toString()}',
            //               name: 'settings.dart'
            //           );
            //           Fluttertoast.showToast(msg: 'An error has occurred');
            //         }
            //       },
            //     ),
            //   ],
            // ),
            const SizedBox(height: 20,),
            Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  createDefaultInputHint(
                    context: context,
                    text: 'DEVICE ID',
                  ),
                  const SizedBox(height: 5,),
                  TextFormField(
                    key: const Key('deviceIDTf'),
                    initialValue: deviceID,
                    onChanged: (val) => deviceID = val,
                    validator: EmptyFieldValidator.validate,
                    decoration: inputTextDecoration.copyWith(
                      hintText: 'Example: XXXXXXXX'
                    ),
                  ),
                  const SizedBox(height: 10,),
                  createDefaultInputHint(
                      context: context,
                      text: 'DEVICE NAME'
                  ),
                  const SizedBox(height: 5,),
                  TextFormField(
                    key: const Key('deviceNameTf'),
                    initialValue: deviceName,
                    onChanged: (val) => deviceName = val,
                    validator: EmptyFieldValidator.validate,
                    decoration: inputTextDecoration.copyWith(
                      hintText: 'Give your device a name!'
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            if (error != '') Text(
              error,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                color: Colors.red,
              ),
            ),
            Row(
              children: [
                Expanded(
                  child: ElevatedButton(
                    child: Text(
                      'Register',
                      style: GoogleFonts.poppins(
                        textStyle: Theme.of(context).textTheme.bodyText1!.copyWith(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    onPressed: () async {
                      setState(() {
                        error = '';
                      });
                      showLoading(context);
                      if (formKey.currentState!.validate()) {

                        try {
                          DatabaseService ds = DatabaseService(widget.userId);
                          bool canRegister = await ds.canRegisterDevice(deviceID);

                          if (canRegister) {
                            await ds.registerDevice(deviceID, deviceName);

                            Navigator.pop(context);
                          } else {
                            setState(() {
                              error = 'Unable to register device. Either the device has been registered or you have entered an invalid ID.';
                            });
                          }
                        } catch(e) {
                          Fluttertoast.showToast(msg: 'An error has occurred');
                          developer.log('Error: ${e.toString()}', name: 'register_device.dart');
                        }
                      }
                      Navigator.pop(context);   // Pop Loading
                    },
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
