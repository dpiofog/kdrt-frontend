import 'package:flutter/material.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:google_fonts/google_fonts.dart';

class ConnectingScreen extends StatefulWidget {

  const ConnectingScreen({Key? key}) : super(key: key);

  @override
  State<ConnectingScreen> createState() => _ConnectingScreenState();
}

class _ConnectingScreenState extends State<ConnectingScreen> {

  bool connecting = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          'Angelica',
          textAlign: TextAlign.center,
        ),
      ),
      body: Container(
        decoration: connecting? const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              gradientTopColor,
              gradientBottomColor,
            ]
          ),
        ) : null,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Expanded(
              flex: 3,
              child: Icon(
                Icons.camera_alt,
                size: 200,
              ),
            ),
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 30),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      connecting? 'Connecting...' : 'Device not found',
                      key: const Key('connecting'),
                      textAlign: TextAlign.center,
                      style: GoogleFonts.dmSans(
                          textStyle: Theme.of(context).textTheme.bodyText1
                      ),
                    ),
                    if (!connecting) const SizedBox(height: 10,),
                    if (!connecting) ElevatedButton(
                      child: const Text(
                          'Try Again'
                      ),
                      onPressed: () {
                        // TODO: retry device connection
                        setState(() {
                          connecting = true;
                        });
                      },
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
