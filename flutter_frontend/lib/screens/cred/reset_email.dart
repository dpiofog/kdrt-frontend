import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_frontend/services/auth.dart';
import 'package:flutter_frontend/services/input_validators.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';

import 'dart:developer' as developer;

import 'package:google_fonts/google_fonts.dart';

class ResetEmailScreen extends StatefulWidget {

  final User user;

  const ResetEmailScreen({
    required this.user,
    Key? key,
  }) : super(key: key);

  @override
  _ResetEmailScreenState createState() => _ResetEmailScreenState();
}

class _ResetEmailScreenState extends State<ResetEmailScreen> {

  final formKey = GlobalKey<FormState>();

  String email = '', password = '', error = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Reset Email'),
        centerTitle: true,
      ),
      body: Form(
        key: formKey,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
          child: ListView(
            children: [
              createDefaultInputHint(
                context: context,
                text: 'CURRENT PASSWORD',
              ),
              const SizedBox(height: 5,),
              TextFormField(
                key: const Key('passwordTF'),
                initialValue: password,
                onChanged: (val) => password = val,
                validator: PasswordFieldValidator.validate,
                decoration: inputTextDecoration,
                obscureText: true,
              ),
              const SizedBox(height: 20,),
              createDefaultInputHint(
                context: context,
                text: 'NEW EMAIL',
              ),
              const SizedBox(height: 5,),
              TextFormField(
                key: const Key('emailTF'),
                initialValue: email,
                onChanged: (val) => email = val,
                validator: EmptyFieldValidator.validate,
                decoration: inputTextDecoration,
              ),
              const SizedBox(height: 10,),
              if (error != '') Text(
                error,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: Colors.red,
                ),
              ),
              Row(
                children: [
                  Expanded(
                    child: ElevatedButton(
                      key: const Key('resetEmailButton'),
                      onPressed: () {
                        setState(() {
                          error = '';
                        });
                        showLoading(context);

                        if (formKey.currentState!.validate()) {
                          AuthService auth = AuthService();
                          auth.changeEmail(
                          widget.user,
                          password,
                          email,
                          ).then((value) {
                            Navigator.pop(context);   // Pop Loading
                            Navigator.pop(context);   // Pop screen back to settings
                          }).catchError((error) {
                            developer.log(
                              'Unable to change user\'s email:',
                              name: 'reset_password.dart',
                              error: error
                            );
                            setState(() {
                              this.error = error.message;
                            });
                            Navigator.pop(context);   // Pop Loading
                          });
                        } else {
                          Navigator.pop(context);   // Pop Loading
                        }
                      },
                      child: Text(
                        'Confirm',
                        style: GoogleFonts.poppins(
                          textStyle: Theme.of(context).textTheme.bodyText1!.copyWith(
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
