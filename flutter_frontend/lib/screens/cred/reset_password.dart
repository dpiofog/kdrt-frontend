import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_frontend/services/auth.dart';
import 'package:flutter_frontend/services/input_validators.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';

import 'dart:developer' as developer;

class ResetPasswordScreen extends StatefulWidget {

  final User user;

  const ResetPasswordScreen({
    required this.user,
    Key? key,
  }) : super(key: key);

  @override
  _ResetPasswordScreenState createState() => _ResetPasswordScreenState();
}

class _ResetPasswordScreenState extends State<ResetPasswordScreen> {

  final formKey = GlobalKey<FormState>();

  String oldPassword = '',
      newPassword = '',
      error = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Reset Password'),
        centerTitle: true,
      ),
      body: Form(
        key: formKey,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
          child: ListView(
            children: [
              createDefaultInputHint(
                context: context,
                text: 'OLD PASSWORD',
              ),
              const SizedBox(height: 5,),
              TextFormField(
                key: const Key('oldPassTF'),
                initialValue: oldPassword,
                onChanged: (val) => oldPassword = val,
                validator: PasswordFieldValidator.validate,
                decoration: inputTextDecoration,
                obscureText: true,
              ),
              const SizedBox(height: 10,),
              createDefaultInputHint(
                context: context,
                text: 'NEW PASSWORD',
              ),
              const SizedBox(height: 5,),
              TextFormField(
                key: const Key('newPassTF'),
                initialValue: newPassword,
                onChanged: (val) => newPassword = val,
                validator: PasswordFieldValidator.validate,
                decoration: inputTextDecoration,
                obscureText: true,
              ),
              const SizedBox(height: 10,),
              createDefaultInputHint(
                context: context,
                text: 'CONFIRM NEW PASSWORD',
              ),
              const SizedBox(height: 5,),
              TextFormField(
                key: const Key('confirmNewPassTF'),
                initialValue: '',
                validator: (val) => ConfirmPasswordFieldValidator.validate(val, newPassword),
                decoration: inputTextDecoration,
                obscureText: true,
              ),
              const SizedBox(height: 10,),
              if (error != '') Text(
                error,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: Colors.red,
                ),
              ),
              Row(
                children: [
                  Expanded(
                    child: ElevatedButton(
                      key: const Key('resetEmailButton'),
                      onPressed: () async {
                        setState(() {
                          error = '';
                        });
                        showLoading(context);
                        if (formKey.currentState!.validate()) {
                          AuthService auth = AuthService();
                          await auth.changePassword(
                            widget.user,
                            oldPassword,
                            newPassword,
                          ).then((value) {
                            Navigator.pop(context);   // Pop Loading
                            Navigator.pop(context);   // Pop screen back to settings
                            Fluttertoast.showToast(msg: 'Password updated');
                          }).catchError((error) {
                            setState(() {
                              this.error = error.message;
                              developer.log(
                                'Unable to change user\'s password:',
                                name: 'reset_password.dart',
                                error: error
                              );
                            });
                            Navigator.pop(context);   // Pop Loading
                          });
                        } else {
                          Navigator.pop(context);   // Pop Loading
                        }
                      },
                      child: Text(
                        'Confirm',
                        style: GoogleFonts.poppins(
                          textStyle: Theme.of(context).textTheme.bodyText1!.copyWith(
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
