import 'dart:io';

import 'package:chewie/chewie.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_frontend/services/database.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:share_plus/share_plus.dart';
import 'package:video_player/video_player.dart';
import 'package:provider/provider.dart';
import 'package:path/path.dart' as path;

import 'dart:developer' as developer;

class DetailsScreen extends StatefulWidget {

  final String fileName;
  final String videoLink;
  final String id;
  final List<String> objects;

  const DetailsScreen({
    Key? key,
    required this.id,
    this.fileName = '',
    this.videoLink = '',
    this.objects = const [],
  }) : super(key: key);

  @override
  State<DetailsScreen> createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {

  late VideoPlayerController _videoPlayerController;
  late ChewieController _chewieController;
  late Future<File> _fetchFileFuture;
  File? videoFile;

  Widget createVideoPlayerWidget(BuildContext context) => FutureBuilder(
    future: _fetchFileFuture,
    builder: (context, snapshot) {
      if (snapshot.connectionState == ConnectionState.done) {
        if (snapshot.hasError) {
          developer.log(
            'Failed to fetch video file',
            name: 'details.dart - createVideoPlayerWidget()',
            error: snapshot.error
          );
          return VideoErrorWidget(errorMsg: snapshot.error.toString(),);
        } else {
          File file = snapshot.data as File;
          videoFile = file;
          _videoPlayerController = VideoPlayerController.file(file);
          // _videoPlayerController = VideoPlayerController.asset('assets/intro.mp4');
          return FutureBuilder(
            future: _videoPlayerController.initialize(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasError) {
                  return VideoErrorWidget(errorMsg: snapshot.error.toString(),);
                } else {
                  _chewieController = ChewieController(
                    videoPlayerController: _videoPlayerController,
                    // autoInitialize: true,
                    autoPlay: false,
                    looping: true,
                    showControls: true,
                  );
                  return AspectRatio(
                    aspectRatio: _videoPlayerController.value.aspectRatio,
                    child: Chewie(
                      controller: _chewieController,
                    ),
                  );
                }
              } else {
                return const VideoLoadingWidget();
              }
            },
          );
        }
      }
      else {
        return const VideoLoadingWidget();
      }
    },
  );

  @override
  void initState() {
    _fetchFileFuture = DefaultCacheManager().getSingleFile(widget.videoLink);

    super.initState();
  }

  @override
  void dispose() {
    try {
      _videoPlayerController.dispose();
    } catch(e) {
      developer.log(e.toString(), name: 'details.dart() - dispose()');
    }

    try {
      _chewieController.dispose();
    } catch(e) {
      developer.log(e.toString(), name: 'details.dart() - dispose()');
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.fileName),
        actions: [
          IconButton(
            icon: const Icon(Icons.delete),
            onPressed: () async {
              await showDialog(
                context: context,
                builder: (context) => AlertDialog(
                  title: const Text('Are you sure?'),
                  content: const Text(
                    'Are you sure you want to delete this recording?'
                  ),
                  actions: [
                    TextButton(
                      onPressed: () {
                        // Pop dialog
                        Navigator.pop(context);
                        // Pop detail screen
                        Navigator.pop(context);
                      },
                      child: const Text('Cancel'),
                    ),
                    TextButton(
                      onPressed: () async {
                        User user = context.read<User?>()!;
                        DatabaseService ds = DatabaseService(user.uid);
                        bool canDelete = await ds.markHistoryForDelete(widget.id);
                        if (canDelete) {
                          Navigator.pop(context);
                        } else {
                          Fluttertoast.showToast(msg: "An error has occurred");
                        }
                      },
                      child: const Text(
                        'OK',
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
          IconButton(
            onPressed: () async {
              if (videoFile != null) {
                String originalPath = videoFile!.path;
                String dir = path.dirname(originalPath);
                String newPath = path.join(dir, 'video.mkv');
                // Change file name
                File newVideoFile = videoFile!.renameSync(newPath);
                await Share.shareFiles([newPath]);
                // Reset file name change
                newVideoFile.renameSync(originalPath);
              }
            },
            icon: const Icon(Icons.share),
          ),
        ],
      ),
      body: Column(
        children: [
          createVideoPlayerWidget(context),
          Expanded(
            flex: 2,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                    child: Text(
                      'DETECTED OBJECTS',
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                  color: sectionHeaderColor,
                ),
                Expanded(
                  child: ListView.separated(
                    itemBuilder: (context, index) => ListTile(
                      title: Text(widget.objects[index]),
                    ),
                    separatorBuilder: (context, index) => const Divider(),
                    itemCount: widget.objects.length,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class VideoErrorWidget extends StatelessWidget {

  final String errorMsg;

  const VideoErrorWidget({
    Key? key,
    this.errorMsg = 'Unknown error',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Container(
        color: Colors.grey[700],
        child: Center(
          child: Text(
            'Unable to play video due to an error: $errorMsg',
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headline4!.copyWith(
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}

class VideoLoadingWidget extends StatelessWidget {
  const VideoLoadingWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Container(
        color: Colors.grey[700],
        child: const Center(
          child: SpinKitRing(color: Colors.white),
        ),
      ),
    );
  }
}

