import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_frontend/models/database_models.dart';
import 'package:flutter_frontend/screens/activity_monitor.dart';
import 'package:flutter_frontend/screens/auth/register_device.dart';
import 'package:flutter_frontend/screens/details.dart';
import 'package:flutter_frontend/screens/settings.dart';
import 'package:flutter_frontend/services/database.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/utility_functions.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import 'dart:developer' as developer;

class HomeScreen extends StatelessWidget {

  final String deviceName;
  final String uptime = '01:09:47:12';   // Might need to change to non-final

  const HomeScreen({
    Key? key,
    this.deviceName = 'Unknown Device'
  }) : super(key: key);

  // Overridden Methods
  @override
  Widget build(BuildContext context) {

    User? user = context.watch<User?>();
    DatabaseService ds = DatabaseService(user!.uid);

    return MultiProvider(
      providers: [
        StreamProvider<UserSettingData>.value(
          value: ds.userSettingData,
          initialData: UserSettingData(),
          catchError: (_, err) {
            developer.log(
                'Error retrieving  user settings data stream: ${err.toString()}',
                name: 'home.dart'
            );
            return UserSettingData();
          },
        ),
        StreamProvider<List<DeviceData>>.value(
          initialData: List<DeviceData>.empty(growable: true),
          value: ds.getDevices(),
          catchError: (_, err) {
            developer.log(
                'Error retrieving list of devices stream: ${err.toString()}',
                name: 'home.dart'
            );
            return [];
          },
        ),
      ],
      child: Builder(
          builder: (context) {
            UserSettingData settings = context.watch<UserSettingData>();
            List<DeviceData> devices = context.watch<List<DeviceData>>();

            return StreamProvider<List<HistoryItem>>.value(
                initialData: List<HistoryItem>.empty(growable: true),
                value: ds.getHistories(deviceId: devices.isNotEmpty? devices[0].deviceId : 'skdala%123klnc'),
                catchError: (_, err) {
                  developer.log(
                      'Error retrieving history stream: ${err.toString()}',
                      name: 'home.dart'
                  );
                  return [];
                },
                child: Builder(
                  builder: (context) {
                    List<HistoryItem> histories = context.watch<List<HistoryItem>>();
                    return Scaffold(
                      appBar: AppBar(
                        title: const Text(appName),
                        centerTitle: true,
                        actions: [
                          IconButton(
                            icon: const Icon(Icons.settings),
                            onPressed: () {
                              // setScreen(context, Screen.settings);
                              Navigator.push(
                                context,
                                createFadingPageRouteBuilder(child: SettingsScreen(
                                  user: user,
                                  userSettings: settings,
                                  registeredDevices: devices,
                                ))
                              );
                            },
                          ),
                        ],
                      ),
                      body: _BodyWidget(histories: histories, devices: devices,),
                    );
                  },
                )
            );
          }
      ),
    );
  }
}

class _BodyWidget extends StatelessWidget {

  final List<HistoryItem> histories;
  final List<DeviceData> devices;

  const _BodyWidget({
    Key? key,
    required this.histories,
    required this.devices
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    User user = context.watch<User?>()!;
    return Column(
      children: [
        Expanded(
          flex: 1,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              devices.isEmpty ? IconButton(
                alignment: Alignment.center,
                iconSize: 100,
                onPressed: () {
                  Navigator.push(
                      context,
                      createFadingPageRouteBuilder(child: RegisterDeviceScreen(userId: user.uid))
                  );
                },
                icon: const Icon(
                  Icons.add_circle_outline,
                ),
              ) : Image.asset(
                'assets/rd1002.png',
                // width: 100,
              ),
              const SizedBox(height: 20,),
              Text(
                devices.isEmpty ? 'No devices registered yet' : 'Connected to ${devices[0].deviceName}',
                style: Theme.of(context).textTheme.headline4,
              ),
              devices.isEmpty? InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      createFadingPageRouteBuilder(child: RegisterDeviceScreen(userId: user.uid))
                  );
                },
                child: Text(
                  'Tap here to add',
                  style: Theme.of(context).textTheme.headline4!.copyWith(
                    color: Colors.blue
                  ),
                ),
              ) : Text(
                'Recordings: ${histories.length}',
                style: Theme.of(context).textTheme.headline4,
              ),
              const SizedBox(height: 15,),
              if (devices.isNotEmpty) ElevatedButton(
                style: roundedButton,
                child: const Text('View Activity'),
                onPressed: () {
                  Navigator.push(
                    context,
                    createFadingPageRouteBuilder(child: ActivityMonitor(histories: histories)),
                  );
                },
              ),
            ],
          ),
        ),
        Expanded(
          flex: 1,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                  child: Text(
                    'HISTORY',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ),
                color: sectionHeaderColor,
              ),
              Expanded(
                child: ListView.separated(
                  itemCount: histories.length,
                  separatorBuilder: (context, index) => const Divider(),
                  itemBuilder: (context, index) {
                    HistoryItem history = histories[index];
                    String formattedDate = formatDate(history.date);
                    return ListTile(
                      title: Text(
                        formattedDate,
                        style: GoogleFonts.poppins(
                            textStyle: Theme.of(context).textTheme.bodyText1
                        ),
                      ),
                      trailing: Text(
                        // formatDuration(history.videoDuration),
                        history.videoDuration,
                        style: GoogleFonts.poppins(
                            textStyle: Theme.of(context).textTheme.bodyText1
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                          context,
                          createFadingPageRouteBuilder(child: DetailsScreen(
                            fileName: formattedDate,
                            videoLink: history.videoLink,
                            objects: history.detectedObjects,
                            id: history.id,
                          )),
                        );
                      },
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

