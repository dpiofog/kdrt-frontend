import 'package:flutter/material.dart';
import 'package:flutter_frontend/screens/auth/login.dart';
import 'package:flutter_frontend/screens/auth/register.dart';
import 'package:flutter_frontend/shared/constants.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';
import 'package:google_fonts/google_fonts.dart';

class LandingScreen extends StatelessWidget {
  const LandingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 60),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(
                child: Text(
                  'Welcome to',
                  key: const Key('welcome'),
                  style: Theme.of(context).textTheme.headline2,
                ),
              ),
              const SizedBox(height: 15,),
              Image.asset(
                'assets/logo.PNG',
                fit: BoxFit.scaleDown,
                key: const Key('logo'),
              ),
              const SizedBox(height: 20,),
              Text(
                appName,
                key: const Key('appName'),
                style: Theme.of(context).textTheme.headline1,
              ),
              const SizedBox(height: 40,),
              Text(
                'A thoughtful combination of design and function to support your everyday safety. Available now.',
                key: const Key('motto'),
                style: Theme.of(context).textTheme.bodyText1,
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 30,),
              Row(
                children: [
                  Expanded(
                    child: ElevatedButton(
                      key: const Key('registerButton'),
                      onPressed: () {
                        Navigator.push(
                          context,
                          PageRouteBuilder(
                            pageBuilder: (pageBuilder, anim1, anim2) =>
                              const RegisterScreen(
                                key: Key('registerScreen'),
                              ),
                            reverseTransitionDuration: const Duration(seconds: 0),
                          )
                        );
                      },
                      child: Text(
                        'Join Now',
                        style: GoogleFonts.poppins(
                          textStyle: Theme.of(context).textTheme.bodyText1!.copyWith(
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      style: roundedButton,
                    ),
                  ),
                  const SizedBox(width: 20,),
                  Expanded(
                    child: ElevatedButton(
                      key: const Key('signInButton'),
                      onPressed: () {
                        Navigator.push(
                            context,
                            PageRouteBuilder(
                              pageBuilder: (pageBuilder, anim1, anim2) =>
                                const LoginScreen(
                                  key: Key('loginScreen'),
                                ),
                              reverseTransitionDuration: const Duration(seconds: 0),
                            ),
                        );
                      },
                      child: Text(
                        'Sign In',
                        style: GoogleFonts.poppins(
                          textStyle: Theme.of(context).textTheme.bodyText1!.copyWith(
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      style: roundedButton.copyWith(
                        backgroundColor: MaterialStateProperty.resolveWith<Color>(
                                (Set<MaterialState> states) {
                              return Theme.of(context).colorScheme.secondary;
                            }
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
