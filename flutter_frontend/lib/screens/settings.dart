import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_frontend/models/database_models.dart';
import 'package:flutter_frontend/screens/cred/reset_email.dart';
import 'package:flutter_frontend/screens/cred/reset_password.dart';
import 'package:flutter_frontend/services/auth.dart';
import 'package:flutter_frontend/services/database.dart';
import 'package:flutter_frontend/shared/widget_prefabs.dart';
import 'package:flutter_settings_ui/flutter_settings_ui.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

import 'dart:developer' as developer;

class SettingsScreen extends StatefulWidget {

  final User user;
  final UserSettingData userSettings;
  final List<DeviceData> registeredDevices;

  const SettingsScreen({
    Key? key,
    required this.user,
    required this.userSettings,
    this.registeredDevices = const [],
  }) : super(key: key);

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {

  bool signedOut = false;

  Future performBackOperation(User user, UserSettingData data) async {
    if (!signedOut) {
      showLoading(context);
      bool isSaved = await DatabaseService(user.uid).updateSettings(data);

      if (isSaved) {
        Fluttertoast.showToast(msg: 'Settings saved');
      } else {
        Fluttertoast.showToast(msg: 'Unable to save settings due to an error');
      }
      Navigator.pop(context);   // Pop Loading
      // setScreen(context, Screen.home);
    }
  }

  @override
  Widget build(BuildContext context) {

    User user = widget.user;
    UserSettingData settings = widget.userSettings;

    return WillPopScope(
      onWillPop: () async {
        await performBackOperation(widget.user, settings);
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text('Settings'),
        ),
        body: SettingsList(
          key: const Key('settingsList'),
          backgroundColor: Colors.white,
          sections: [
            SettingsSection(
              title: 'Account',
              titlePadding: const EdgeInsets.fromLTRB(16, 10, 0, 0),
              tiles: [
                SettingsTile(
                  title: 'Username',
                  trailing: Text(user.displayName ?? ''),
                ),
                // FIXME: Resetting email prevents the user to sign in, requiring them to reset their password
                // SettingsTile(
                //   title: 'Reset Email',
                //   onPressed: (context) {
                //
                //     User? user = context.read<User?>();
                //
                //     if (user != null) {
                //       Navigator.push(
                //         context,
                //         createFadingPageRouteBuilder(child: ResetEmailScreen(user: user))
                //       );
                //     }
                //   },
                // ),
                SettingsTile(
                  title: 'Reset Password',
                  onPressed: (context) {

                    User? user = context.read<User?>();

                    if (user != null) {
                      Navigator.push(
                        context,
                        createFadingPageRouteBuilder(child: ResetPasswordScreen(user: user))
                      );
                    }
                  },
                ),
              ],
            ),
            SettingsSection(
              title: 'System',
              tiles: [
                // SettingsTile(
                //   title: 'Enable System',
                //   trailing: Switch(
                //     value: settings.enableSystem,
                //     onChanged: (enabled) {
                //       // TODO: Enable/Disable system
                //       setState(() {
                //         settings.enableSystem = enabled;
                //       });
                //     }
                //   ),
                // ),
                // SettingsTile(
                //   title: 'Enable Mobile Data Usage',
                //   trailing: Switch(
                //       value: settings.enableMobileData,
                //       onChanged: (enabled) {
                //         // TODO: Enable/Disable system
                //         setState(() {
                //           settings.enableMobileData = enabled;
                //         });
                //       }
                //   ),
                // ),
                SettingsTile(
                  title: 'Unregister Device',
                  onPressed: (context) async {
                    if (widget.registeredDevices.isNotEmpty) {
                      await showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                        title: const Text('Are you sure?'),
                        content: const Text(
                            'Are you sure you want to deregister your current device?'
                            ' You will lose all recordings that was recorded from that device.'
                        ),
                        actions: [
                          TextButton(
                            onPressed: () {
                              // Pop dialog
                              Navigator.pop(context);
                            },
                            child: const Text('Cancel'),
                          ),
                          TextButton(
                            onPressed: () async {
                              showLoading(context);
                              DatabaseService ds = DatabaseService(widget.user.uid);

                              try {
                                await ds.unregisterDevice(widget.registeredDevices[0].deviceId);
                                widget.registeredDevices.removeAt(0);
                                Fluttertoast.showToast(msg: 'Device unregistered successfully');
                              } catch(e) {
                                Fluttertoast.showToast(msg: 'Failed to unregister device due to an error');
                                  developer.log('Failed to unregister device due to an error:',
                                    name: 'settings.dart()',
                                    error: e
                                );
                              }

                              // Pop loading and dialog box
                              Navigator.pop(context);
                              Navigator.pop(context);
                            },
                            child: const Text(
                                'Yes, I\'m sure'
                            ),
                          ),
                        ],
                      ),
                    );
                    } else {
                      Fluttertoast.showToast(msg: 'No registered device');
                    }
                  },
                ),
              ],
            ),
            SettingsSection(
              title: 'Other',
              tiles: [
                SettingsTile(
                  title: 'Sign Out',
                  onPressed: (context) async {
                    developer.log('Signing out user...', name: 'settings.dart');
                    try {
                      AuthService auth = AuthService();
                      await auth.signOut();
                      signedOut = true;
                      Navigator.pop(context);
                    } catch(e) {
                      developer.log('Unable to sign out user: ${e.toString()}',
                        name: 'settings.dart'
                      );
                    }
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
