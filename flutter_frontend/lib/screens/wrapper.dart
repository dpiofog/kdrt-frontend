import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_frontend/screens/home.dart';
import 'package:flutter_frontend/screens/landing.dart';
import 'package:flutter_frontend/shared/screen_state.dart';
import 'package:provider/provider.dart';

import 'dart:developer' as developer;

class Wrapper extends StatelessWidget {
  const Wrapper({Key? key}) : super(key: key);

  Widget getActiveScreen(Screen activeScreen) {
    switch (activeScreen) {

      case Screen.home:
        return const HomeScreen();

      // case Screen.registerDevice:
      //   return const RegisterDeviceScreen();

      // case Screen.settings:
      //   return const SettingsScreen();

      default:
        return const LandingScreen();
    }
  }

  @override
  Widget build(BuildContext context) {

    User? user = context.watch<User?>();   // Get user login stream
    ScreenState screenState = context.watch<ScreenState>();   // Get screen state stream
    Screen activeScreen = screenState.activeScreen;

    developer.log('user is null : ${user == null}', name: 'wrapper.dart');

    if (user == null) {
      return const LandingScreen();
    } else {
      return getActiveScreen(activeScreen);
    }
  }
}
