import 'package:firebase_auth/firebase_auth.dart';

import 'dart:developer' as developer;

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  /// auth change user stream
  Stream<User?> get user {
    return _auth.authStateChanges();
  }

  /// sign in with email and password
  Future signIn(String email, String password) async {
    try {
      UserCredential result = await _auth.signInWithEmailAndPassword(email: email, password: password);
      User? user = result.user;

      if (user!.emailVerified) {
        return user;
      } else {
        return null;
      }
    } catch (e) {
      developer.log(e.toString(), name: 'auth.dart - signIn()');
      return null;
    }
  }

  /// register with email and password
  Future register({
    required String email,
    required String password,
    required String username,
  }) async {
    try {
      developer.log('Registering a new user to firebase...', name: 'auth.dart - register()');
      UserCredential result = await _auth.createUserWithEmailAndPassword(email: email, password: password);
      User? user = result.user;

      // Update display name
      developer.log('Updating display name...', name: 'auth.dart - register()');
      await user!.updateDisplayName(username);

      // Send email verification
      developer.log('Sending email verification...', name: 'auth.dart - register()');
      await user.sendEmailVerification();

      developer.log('Registering process complete!', name: 'auth.dart - register()');

      return user;
    } catch(e) {
      developer.log(e.toString(), name: 'auth.dart - register()');
      return null;
    }
  }

  /// sign out
  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch (e) {
      developer.log(e.toString(), name: 'auth.dart - signOut()');
      return null;
    }
  }

  /// Change the password of the currently logged in user.
  Future changePassword(User user, String oldPass, String newPass) async {
    final cred = EmailAuthProvider.credential(email: user.email!, password: oldPass);

    await user.reauthenticateWithCredential(cred).then((value) async {
      await user.updatePassword(newPass);
    });
  }

  /// Function for 'forgot password'.
  Future resetPassword(String email) async {
    await _auth.sendPasswordResetEmail(email: email);
  }

  /// Change the email of the currently logged in user.
  Future changeEmail(User user, String password, String newEmail) async {
    final cred = EmailAuthProvider.credential(
      email: user.email!,
      password: password
    );

    await user.reauthenticateWithCredential(cred).then((value) async {
      await user.updateEmail(newEmail);
      await user.updatePassword(password);
    });
  }
}