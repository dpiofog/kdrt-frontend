import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_frontend/shared/extensions.dart';
import 'dart:developer' as developer;

import 'package:flutter_frontend/models/database_models.dart';

class DatabaseService {

  // Fields
  final String uid;
  final FirebaseFirestore _firebase = FirebaseFirestore.instance;
  late DocumentReference userSettingDoc;
  late CollectionReference historyCollection, deviceCollection;

  static const String userId = 'user_id',
    deviceName = 'device_name',
    deviceId = 'device_id',
    enableSystem = 'enable_sys',
    enableMobileData = 'enable_md',
    date = 'date',
    videoLink = 'link',
    videoDuration = 'duration',
    detectedObjects = 'objects',
    isDeleted = 'is_delete';

  // Constructor
  DatabaseService(this.uid) {
    userSettingDoc = _firebase.collection('users').doc(uid);
    historyCollection = _firebase.collection('history');
    deviceCollection = _firebase.collection('devices');
  }

  // Snapshot to Database model classes
  /// Convert from document snapshot to UserSettingData object
  UserSettingData userSettingFromSnapshot(DocumentSnapshot snapshot) {
    Map<String, dynamic> data = snapshot.data()! as Map<String, dynamic>;

    bool? emd = data[enableMobileData];
    bool? es = data[enableSystem];

    return UserSettingData(
      enableMobileData: emd ?? false,
      enableSystem: es ?? true,
    );
  }

  /// Convert from document snapshot to DeviceData object
  DeviceData deviceFromSnapshot(DocumentSnapshot snapshot) {
    Map<String, dynamic> data = snapshot.data()! as Map<String, dynamic>;

    return DeviceData(
      deviceId: snapshot.id,
      userId: data[userId] ?? '',
      deviceName: data[deviceName] ?? '',
    );
  }

  /// Convert from document snapshot to DeviceData object
  HistoryItem historyItemFromSnapshot(DocumentSnapshot snapshot) {
    Map<String, dynamic> data = snapshot.data()! as Map<String, dynamic>;

    double timestamp = data[date] * 1000;
    var objectMap = data[detectedObjects] ?? <String, dynamic>{};
    List<String> objects = [];

    for (String key in objectMap.keys) {
      int val = objectMap[key].round();
      if (val > 0) objects.add('${key.capitalize()} ($val)');
    }

    return HistoryItem(
      id: snapshot.id,
      userId: data[userId] ?? '',
      deviceId: data[deviceId] ?? '',
      // date: DateTime.parse(data[date].toDate().toString()),
      date: DateTime.fromMillisecondsSinceEpoch(timestamp.round()),
      videoLink: data[videoLink] ?? '',
      // videoDuration: Duration(seconds: data[videoDuration] ?? 0),
      videoDuration: data[videoDuration],
      detectedObjects: objects
    );
  }

  /// Create the document for user setting data for the current user
  Future<bool> initUserSettingData(UserSettingData data) async {
    developer.log(
      'Creating new user setting data for user $uid',
      name: 'database.dart - initUserSettingData()'
    );
    bool isSuccess = false;
    await userSettingDoc.set({
      enableSystem : data.enableSystem,
      enableMobileData : data.enableMobileData,
    }).then((value) {
      isSuccess = true;
    }).catchError((error) {
      developer.log(
        'Error creating new user setting data for user $uid: ${error.toString()}',
        name: 'database.dart - initUserSettingData()'
      );
      isSuccess = false;
    });
    return isSuccess;
  }

  // /// Create the document for device data for the current user
  // Future<bool> initDeviceData(DeviceData data) async {
  //   developer.log(
  //       'Creating new device data for user $uid',
  //       name: 'database.dart - initUserSettingData()'
  //   );
  //   await deviceDoc.set({
  //     userId : data.userId,
  //     deviceName : data.deviceName,
  //   }).then((value) {
  //     return true;
  //   }).catchError((error) {
  //     developer.log(
  //         'Error creating new device data for user $uid: ${error.toString()}',
  //         name: 'database.dart - initDeviceData()'
  //     );
  //     return false;
  //   });
  //   developer.log(
  //       'Something went wrong',
  //       name: 'database.dart - initDeviceData()'
  //   );
  //   return false;
  // }

  /// Add new history item to the database
  Future<bool> addHistoryItem(HistoryItem data) async {
    bool isSuccess = false;
    developer.log(
        'Adding new history item for user $uid',
        name: 'database.dart - addHistoryItem()'
    );
    await historyCollection.doc().set({
      userId : data.userId,
      videoLink : data.videoLink,
      date : data.date,
      detectedObjects : data.detectedObjects,
    }).then((value) {
      isSuccess = true;
    }).catchError((error) {
      developer.log(
          'Error creating new history item for user $uid: ${error.toString()}',
          name: 'database.dart - addHistoryItem()'
      );
      isSuccess = false;
    });
    return isSuccess;
  }

  /// Get a stream of the history
  /// [deviceId] The ID of the device. Leave null to query all history from the user.
  Stream<List<HistoryItem>>? getHistories({String? deviceId}) {
    try {
      developer.log('Getting history stream', name: 'database.dart - getHistories()');
      Query query = historyCollection.where(
        isDeleted,
        isEqualTo: false,
      ).where(
        userId,
        isEqualTo: uid,
      );

      if (deviceId != null) {
        query = query.where(
          DatabaseService.deviceId,
          isEqualTo: deviceId,
        );
      }

      query = query.orderBy(
          date,
          descending: true
      );

      Stream<QuerySnapshot>? queryStream = query.snapshots();

      developer.log(
          'Converting stream to HistoryItem list stream',
          name: 'database.dart - getHistories()'
      );

      return queryStream.map((querySnapshot) =>
          querySnapshot.docs.map(historyItemFromSnapshot).toList()
      );
    } catch(e) {
      developer.log(e.toString(), name: 'database.dart - getHistories()');
      return null;
    }
  }

  /// Get a stream of devices registered to the user
  Stream<List<DeviceData>>? getDevices() {
    try {
      developer.log('Getting device stream', name: 'database.dart - getDevices()');
      Stream<QuerySnapshot>? queryStream = deviceCollection.where(
          userId,
          isEqualTo: uid
      ).snapshots();

      developer.log(
          'Converting stream to DeviceData list stream',
          name: 'database.dart - getDevices()'
      );

      return queryStream.map((querySnapshot) =>
          querySnapshot.docs.map(deviceFromSnapshot).toList()
      );
    } catch(e) {
      developer.log('Error: ${e.toString()}', name: 'database.dart - getDevices()');
      return null;
    }
  }

  /// Check if device can be registered
  Future<bool> canRegisterDevice(String deviceId) async {
    DocumentSnapshot doc = await deviceCollection.doc(deviceId).get();

    if (doc.exists) {
      DeviceData data = deviceFromSnapshot(doc);
      return data.userId == '';
    } else {
      return false;
    }
  }

  /// Register a device for the user
  Future registerDevice(String deviceId, String deviceName) async {
    developer.log('Registering device...', name: 'database.dart - registerDevice()');
    DocumentReference doc = deviceCollection.doc(deviceId);
    await doc.set({
      userId : uid,
      DatabaseService.deviceName: deviceName
    });
    developer.log('Device registered', name: 'database.dart - registerDevice()');
  }

  /// Deregister a device from the user
  Future unregisterDevice(String deviceId) async {
    developer.log('Unregistering device...', name: 'database.dart - unregisterDevice()');
    DocumentReference doc = deviceCollection.doc(deviceId);
    await doc.set({
      userId : '',
      DatabaseService.deviceName: ''
    });
    developer.log('Device unregistered', name: 'database.dart - unregisterDevice()');
  }

  /// Add a new device to the database (it does not register the device
  /// to the user
  Future addNewDevice({String? id}) async {
    developer.log(
      'Adding new device...',
      name: 'database.dart - addNewDevice()',
    );

    DocumentReference doc = deviceCollection.doc(id);

    // Check if device document already exists
    DocumentSnapshot docSnapshot = await doc.get();
    if (!docSnapshot.exists) {
      await doc.set({
        deviceName : '',
        userId : ''
      });
      developer.log(
        'New device added successfully!',
        name: 'database.dart - addNewDevice()',
      );
    } else {
      developer.log(
        'New device not added: Device already exists!',
        name: 'database.dart - addNewDevice()',
      );
    }
  }

  /// Get the stream to the user settings data document
  Stream<UserSettingData>? get userSettingData {
    try {
      return userSettingDoc.snapshots().map(userSettingFromSnapshot);
    } catch(e) {
      developer.log(
        'Unable to get user $uid settings data: ${e.toString()}',
        name: 'database.dart - getUserSettingData()'
      );
      return null;
    }
  }

  /// Update the user settings
  Future<bool> updateSettings(UserSettingData data) async {
    bool isSuccess = false;
    await userSettingDoc.update({
      enableSystem : data.enableSystem,
      enableMobileData : data.enableMobileData
    }).then((value) {
      isSuccess = true;
    }).catchError((error) {
      developer.log(
        'Unable to save user $uid settings data: ${error.toString()}',
        name: 'database.dart - updateSettings()'
      );
      initUserSettingData(data).then((value) {
        isSuccess = true;
      }).catchError((error2) {
        developer.log(
            'Unable to create user $uid settings data document: ${error.toString()}',
            name: 'database.dart - updateSettings()'
        );
        isSuccess = false;
      });
    });

    return isSuccess;
  }

  /// Mark the selected history for deletion
  /// Returns true if the history can be deleted.
  Future<bool> markHistoryForDelete(String docId) async {
    try {
      await historyCollection.doc(docId).delete();
      return true;
    } catch (e) {
      developer.log(
        'Unable to delete history due to an error: ${e.toString()}',
        name: 'database.dart - markHistoryForDelete()',
      );
      return false;
    }
  }
}