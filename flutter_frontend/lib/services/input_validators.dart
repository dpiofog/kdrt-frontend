import 'package:flutter_frontend/shared/utility_functions.dart';

class EmptyFieldValidator {
  static String? validate(String? val) =>
      makeNullStringEmpty(val).isEmpty? 'Field cannot be empty' : null;
}

class EmailFieldValidator {
  static String? validate(String? val) =>
      makeNullStringEmpty(val).isEmpty? 'Field cannot be empty' : null;
}

class PasswordFieldValidator {
  static String? validate(String? val) =>
      makeNullStringEmpty(val).length < 6? 'Password too short' : null;
}

class ConfirmPasswordFieldValidator {
  /// Checks if the second password matches the first.
  static String? validate(String? val, String firstPass) =>
      makeNullStringEmpty(val) != firstPass? 'Password does not match' : null;
}