import 'package:flutter/material.dart';

const String appName = 'Angelica';

const Color mainColor = Color.fromRGBO(255, 111, 111, 1.0);
const Color secondaryButtonColor = Colors.black;
const Color hyperlinkColor = Color.fromRGBO(61, 184, 255, 1.0);
const Color sectionHeaderColor = Color.fromRGBO(206, 206, 206, 1.0);
const Color gradientTopColor = Color.fromRGBO(231, 122, 162, 1.0);
const Color gradientBottomColor = Color.fromRGBO(101, 132, 214, 1.0);