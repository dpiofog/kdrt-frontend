import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'constants.dart';

class CustomTheme {
  static ThemeData get lightTheme {
    return ThemeData(
      scaffoldBackgroundColor: Colors.white,
      textTheme: TextTheme(
        headline1: GoogleFonts.poppins(
          textStyle: const TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 32,
            color: Colors.black,
          ),
        ),
        headline2: GoogleFonts.poppins(
          textStyle: const TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 20,
            color: Colors.black,
          ),
        ),
        headline3: GoogleFonts.poppins(
          textStyle: const TextStyle(
            fontSize: 18,
            color: Colors.black,
          ),
        ),
        headline4: GoogleFonts.dmSans(
          textStyle: const TextStyle(
            fontSize: 16,
            color: Colors.black,
          ),
        ),
        bodyText1: GoogleFonts.dmSans(
          textStyle: const TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.normal,
            color: Colors.black,
          ),
        ),
      ),
      primaryColor: mainColor,
      colorScheme: ColorScheme.fromSwatch().copyWith(
        primary: mainColor,
        secondary: Colors.black,
      ),
    );
  }
}