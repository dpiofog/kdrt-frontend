/// Makes the first letter of the string capital, while the rest
/// is in lowercase.
extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${substring(1).toLowerCase()}";
  }
}