import 'package:flutter/material.dart';

enum Screen {
  login,
  register,
  landing,
  connecting,
  home,
  settings,
  details,
  // registerDevice,
}

class ScreenState with ChangeNotifier {
  Screen _activeScreen = Screen.landing;

  /// Get the currently active screen value
  Screen get activeScreen => _activeScreen;

  /// Update the screen tracker with a new value
  void set(Screen screen) {
    _activeScreen = screen;
    notifyListeners();
  }
}