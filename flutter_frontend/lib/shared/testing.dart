import 'package:flutter/material.dart';

import 'custom_theme.dart';

Widget makeWidgetTestable(Widget child){
  return MaterialApp(
    debugShowCheckedModeBanner: false,
    home: child,
    theme: CustomTheme.lightTheme,
  );
}