import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import 'screen_state.dart';

import 'dart:developer' as developer;

/// Function to convert String? to String by changing the String to empty
/// String if it is null.
String makeNullStringEmpty(String? str) => str ?? '';

/// Shorthand function to update the ScreenState provider
/// and set the displayed screen.
///
/// This function is to be called outside of the Wrapper class.
///
/// It uses context.read<ScreenState>() to get the ScreenState object.
void setScreen(BuildContext? context, Screen newScreen) {
  if (context != null) {
    try {
      context.read<ScreenState>().set(newScreen);
    } catch (e) {
      developer.log(e.toString(), name: 'utility_functions.dart - setScreen()');
    }
  }
}

/// Format DateTime object in to String
String formatDate(DateTime date) {
  final DateFormat formatter = DateFormat.yMd().add_jm();
  return formatter.format(date);
}

/// Format DateTime object in to String (no time)
String formatDateNoTime(DateTime date) {
  final DateFormat formatter = DateFormat.yMd();
  return formatter.format(date);
}

/// Formats Duration object to string with format
/// HH:mm:ss
String formatDuration(Duration duration) {
  // Code retrieved from
  // https://stackoverflow.com/questions/54775097/formatting-a-duration-like-hhmmss
  String twoDigits(int n) => n.toString().padLeft(2, "0");
  String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
  String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
  return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
}