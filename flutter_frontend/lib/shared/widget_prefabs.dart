import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';

/// Create a PageRouteBuilder equipped with fading transition.
PageRouteBuilder createFadingPageRouteBuilder({
  required Widget child,
  Duration fadeDuration = const Duration(milliseconds: 200),
}) => PageRouteBuilder(
  pageBuilder: (context, a1, a2) => child,
  transitionsBuilder: (context, anim, a2, child) => FadeTransition(opacity: anim, child: child),
  transitionDuration: fadeDuration,
);

/// Button style to make buttons have rounded corners
ButtonStyle roundedButton = ButtonStyle(
  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
    RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(18.0),
    ),
  ),
);

/// Decoration to make text fields have outlines
InputDecoration inputTextDecoration = const InputDecoration(
  border: OutlineInputBorder(),
);

Widget createDefaultInputHint({
  required BuildContext context,
  Key? key,
  String text = '',
}) => Text(
  text,
  key: key,
  style: GoogleFonts.dmSans(
      textStyle: Theme.of(context).textTheme.bodyText1
  )
);

/// Show the loading widget in a dialog
void showLoading(BuildContext context) {
  showDialog(
      context: context,
      barrierDismissible: false,
      useSafeArea: true,
      builder: (context) {
        return const Loading();
      }
  );
}

class Loading extends StatelessWidget {
  const Loading({Key? key}) : super(key: key);

  Widget _createSpinKitWidget() {
    return Center(
      child: SpinKitFadingCircle(
        color: Colors.blue[400],
        size: 50.0,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: _createSpinKitWidget(),
    );
  }
}