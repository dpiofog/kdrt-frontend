import 'package:flutter/material.dart';
import 'package:flutter_frontend/screens/landing.dart';
import 'package:flutter_frontend/shared/testing.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('Landing screen widget test - ', () {
    testWidgets('Check for welcome text widget existence', (WidgetTester tester) async {
      await tester.pumpWidget(makeWidgetTestable(const LandingScreen()));
      await tester.pumpAndSettle();

      // Create finders
      final finder = find.byKey(const Key('welcome'));

      expect(finder, findsOneWidget);
    });

    testWidgets('Check for logo widget existence', (WidgetTester tester) async {
      await tester.pumpWidget(makeWidgetTestable(const LandingScreen()));
      await tester.pumpAndSettle();

      // Create finders
      final finder = find.byKey(const Key('logo'));
      final finder2 = find.text('Welcome to');

      expect(finder, findsOneWidget);
      expect(finder2, findsOneWidget);
    });

    testWidgets('Check for app title widget existence', (WidgetTester tester) async {
      await tester.pumpWidget(makeWidgetTestable(const LandingScreen()));
      await tester.pumpAndSettle();

      // Create finders
      final finder = find.byKey(const Key('appName'));

      expect(finder, findsOneWidget);
    });

    testWidgets('Check for motto widget existence', (WidgetTester tester) async {
      await tester.pumpWidget(makeWidgetTestable(const LandingScreen()));
      await tester.pumpAndSettle();

      // Create finders
      final finder = find.byKey(const Key('motto'));
      final finder2 = find.text('Angelica');

      expect(finder, findsOneWidget);
      expect(finder2, findsOneWidget);
    });

    testWidgets('Check for register button existence', (WidgetTester tester) async {
      await tester.pumpWidget(makeWidgetTestable(const LandingScreen()));
      await tester.pumpAndSettle();

      // Create finders
      final finder = find.byKey(const Key('registerButton'));
      final finder2 = find.text('Join Now');

      expect(finder, findsOneWidget);
      expect(finder2, findsOneWidget);
    });

    testWidgets('Check for sign in button existence', (WidgetTester tester) async {
      await tester.pumpWidget(makeWidgetTestable(const LandingScreen()));
      await tester.pumpAndSettle();

      // Create finders
      final finder = find.byKey(const Key('signInButton'));
      final finder2 = find.text('Sign In');

      expect(finder, findsOneWidget);
      expect(finder2, findsOneWidget);
    });

    testWidgets('Pressing on the Join Now button opens the register screen', (WidgetTester tester) async {
      await tester.pumpWidget(makeWidgetTestable(const LandingScreen()));
      await tester.pumpAndSettle();

      final buttonFinder = find.byKey(const Key('registerButton'));
      expect(buttonFinder, findsOneWidget);

      await tester.tap(buttonFinder);
      await tester.pumpAndSettle();

      // The register screen should appear
      final finder = find.byKey(const Key('registerScreen'));
      expect(finder, findsOneWidget);
    });

    testWidgets('Pressing on the Sign In button opens the login screen', (WidgetTester tester) async {
      await tester.pumpWidget(makeWidgetTestable(const LandingScreen()));
      await tester.pumpAndSettle();

      final buttonFinder = find.byKey(const Key('signInButton'));
      expect(buttonFinder, findsOneWidget);

      await tester.tap(buttonFinder);
      await tester.pumpAndSettle();

      // The register screen should appear
      final finder = find.byKey(const Key('loginScreen'));
      expect(finder, findsOneWidget);
    });
  });
}