import 'package:flutter_frontend/screens/auth/register.dart';
import 'package:flutter_frontend/shared/testing.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';

void main() {
  group('Register screen widget test - ', () {
    testWidgets('Check for widget existence', (WidgetTester tester) async {
      await tester.pumpWidget(makeWidgetTestable(const RegisterScreen()));
      await tester.pumpAndSettle();

      final titleFinder = find.byKey(const Key('createNewAccount'));
      final alreadyRegisteredFinder = find.byKey(const Key('alreadyRegistered'));
      final signInHereFinder = find.byKey(const Key('signInHere'));
      final listViewFinder = find.byKey(const Key('listView'));
      final wrapFinder = find.byKey(const Key('wrap'));
      final usernameHintFinder = find.byKey(const Key('usernameHint'));
      final usernameTFFinder = find.byKey(const Key('usernameTF'));
      final emailHintFinder = find.byKey(const Key('emailHint'));
      final emailTFFinder = find.byKey(const Key('emailTF'));
      final passwordHintFinder = find.byKey(const Key('passwordHint'));
      final passwordTFFinder = find.byKey(const Key('passwordTF'));
      final confirmPasswordHintFinder = find.byKey(const Key('confirmPasswordHint'));
      final confirmPasswordTFFinder = find.byKey(const Key('confirmPasswordTF'));
      final signUpButtonFinder = find.byKey(const Key('signUpButton'));

      expect(titleFinder, findsOneWidget);
      expect(alreadyRegisteredFinder, findsOneWidget);
      expect(signInHereFinder, findsOneWidget);
      expect(listViewFinder, findsOneWidget);
      expect(wrapFinder, findsOneWidget);
      expect(usernameHintFinder, findsOneWidget);
      expect(usernameTFFinder, findsOneWidget);
      expect(emailHintFinder, findsOneWidget);
      expect(emailTFFinder, findsOneWidget);
      expect(passwordHintFinder, findsOneWidget);
      expect(passwordTFFinder, findsOneWidget);
      expect(confirmPasswordHintFinder, findsOneWidget);
      expect(confirmPasswordTFFinder, findsOneWidget);
      expect(signUpButtonFinder, findsOneWidget);
    });
  });
}